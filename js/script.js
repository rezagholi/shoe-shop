
document.getElementById("defaultOpen").click();
function modal_show() {
    $(".select-size-modal").addClass("modal-show");
    $("body").addClass("prevent-body");
    document.getElementsByClassName("balck-overlay")[0].style.display = "block";
}
function modal_close() {
    $(".select-size-modal").removeClass("modal-show");
    $("body").removeClass("prevent-body");
    document.getElementsByClassName("balck-overlay")[0].style.display = "none";
}
$("body").click(function (event) {
    if (!$(event.target).closest(".select-size-modal,.s-guide").length) {
        console.log("out of modal clicked");
        modal_close();
    }
});
document.getElementsByClassName("close-btn")[0].addEventListener("click", function () {
    modal_close();
});
document.getElementsByClassName("s-guide")[0].addEventListener("click", function (event) {
    event.preventDefault();
    modal_show();
});

function openNav() {
    var x = document.getElementsByClassName("ham-container");
    x[0].classList.toggle("is-active");
    // document.getElementById("tog").style.display = "block";
    document.getElementById("tog").classList.toggle("open-close");
}


function opentab(evt, tabname) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabname).style.display = "block";
    evt.currentTarget.className += " active";
}
$(document).ready(function () {
    $(".image-slide-container img").click(function () {
        $(".image-container img").attr("src", $(this).attr("src"));
    });
    $(".like-pro-btn").click(function () {
        $(this).find("i").toggleClass("fav");
    });
    $(".p-color").click(function (e) {
        e.preventDefault();
        $(".p-color").removeClass("active");
        $(this).addClass("active");
    });
    $(".review span").hover(function () {
        $(this).addClass("active tr");
        $(this).nextAll(".rv").removeClass("active tr");
        $(this).prevAll(".rv").addClass("active tr");
    });
    $(".review").mouseleave(function () {
        $(".review span").addClass("active");
        $(".review span").removeClass("tr")
    });

    $(".review span").click(function () {
        $(this).addClass("sabt");
        $(this).nextAll().removeClass("sabt");
        $(this).prevAll().addClass("sabt");
    });

    // code for animating slide show
    // $(".image-slide-container img").click(function(){
    //     $(".image-container").css({
    //         "background-image": "url(" + $(this).attr("src") + ")"
    //     }, 'slow');
    // });

    $("#size-select").chosen({ rtl: true, "disable_search_threshold": 5 });
    // $(".chosen-select").chosen({"disable_search_threshold": 4 });

});
